import React, { Component } from 'react';
import baby from '../img/baby.ico';
import Boy from '../img/boy.png';
import Girl from '../img/girl.png';
import Unisex from '../img/unisex.png';
import '../css/App.css';
import { Button, Icon, Navbar, NavItem, Row, Col, Input, Collapsible, CollapsibleItem, Pagination, Modal } from 'react-materialize'
import Typing from 'react-typing-animation';
import Loader from 'react-loader-spinner'
import InfiniteScroll from 'react-infinite-scroller';
import SweetAlert from 'sweetalert-react';
import '../../node_modules/sweetalert/dist/sweetalert.css'
import { Helmet } from "react-helmet";


var countries = require('country-list')();

var religions = [
  "Hindu", "Buddhism", "Christianity", "Greek", "Islam", "Jainism", "Judaism", "Shinto", "Sikh", "Zoroastrianism"
]

class App extends Component {


  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      data: [],
      pageSize: 5,
      resp: "No",
      currentPage: 1,
      loadNext: false,
      listLoaded: false,
      letters: [],
      first: "B",
      gender: "Girl",
      religions: religions,
      selectedReligion: "Hindu",
      hasNextData: true,
      show: false,
      email: null
    }

    this._load = this._load.bind(this)
    this._selectFirst = this._selectFirst.bind(this)
    this._selectGender = this._selectGender.bind(this)
    this._selectReligion = this._selectReligion.bind(this)

  }

  async componentDidMount() {

    const response = await fetch('/hello?page=' + this.state.currentPage + '&first=' + this.state.first + '&gender=' + this.state.gender + '&religion=' + this.state.selectedReligion);
    const body = await response.json();

    this.setState({
      data: body.data,
      listLoaded: true
    })
    var letters = this.genCharArray('A', 'Z');

    var nameObj = countries.getNameList();
    var keys = Object.keys(nameObj)
    this.setState({
      countries: keys,
      letters: letters,
      isLoading: false
    })

  }
  genCharArray(charA, charZ) {
    var a = [], i = charA.charCodeAt(0), j = charZ.charCodeAt(0);
    for (; i <= j; ++i) {
      a.push(String.fromCharCode(i));
    }
    return a;
  }


  async _load() {
    var old = this.state.currentPage;
    this.setState({
      currentPage: old + 1,
      loadNext: true,
      showLoader: true
    })

    const response = await fetch('/hello?page=' + this.state.currentPage + '&first=' + this.state.first + '&gender=' + this.state.gender + '&religion=' + this.state.selectedReligion);
    const body = await response.json();

    if ((body.data).length > 0) {
      console.log("yes")
    } else {
      console.log("no")
      this.setState({
        hasNextData: false
      })
    }

    var data = this.state.data
    data = data.concat(body.data)

    this.setState({
      data: data,
      loadNext: false
    })

  }
  async _selectFirst(first) {
    this.setState({
      currentPage: 1,
      first: first,
      listLoaded: false,
      hasNextData: true
    })

    const response = await fetch('/hello?page=' + this.state.currentPage + '&first=' + first + '&gender=' + this.state.gender + '&religion=' + this.state.selectedReligion);
    const body = await response.json();

    this.setState({
      data: body.data,
      listLoaded: true
    })

  }
  async _selectGender(g) {
    var gender = g
    this.setState({
      currentPage: 1,
      gender: gender,
      listLoaded: false,
      hasNextData: true
    })

    const response = await fetch('/hello?page=' + this.state.currentPage + '&first=' + this.state.first + '&gender=' + gender + '&religion=' + this.state.selectedReligion);
    const body = await response.json();

    console.log(body.data)
    console.log(this.state.currentPage, gender)
    this.setState({
      data: body.data,
      listLoaded: true
    })

  }
  async _selectReligion(r) {
    // var r = e.target.value 
    this.setState({
      currentPage: 1,
      selectedReligion: r,
      listLoaded: false,
      hasNextData: true
    })
    console.log(r)
    const response = await fetch('/hello?page=' + this.state.currentPage + '&first=' + this.state.first + '&gender=' + this.state.gender + '&religion=' + r);
    const body = await response.json();

    this.setState({
      data: body.data,
      listLoaded: true
    })

  }

  render() {
    return (
      <div className="App">
        <Helmet>
          <meta charSet="utf-8" />
          <title>Baby names latest </title>
          <link rel="canonical" href="http://babynames80000.herokuapp.com" />
          <meta name="description" content="latest baby names all religion " />
        </Helmet>
        {
          this.state.isLoading ?
            <div className="loader2">
              <Loader

                type="Circles"
                color="#00BFFF"
                height="100"
                width="100"
              />
            </div> :
            <div>
              <Row className="header">
                <Col s={12} className="center">
                  <span style={{ color: '#fff', fontSize: '50px', paddingTop: '0px', fontFamily: 'titleFonts' }}>Baby Names</span>
                </Col>
                <Col s={12} className="center">
                  <img src={require('../img/baby.ico')} className="baby" />
                </Col>
                <Col s={12} className="center">
                  <span className="titleMsg"> Welcome to the parents junction,<br />here you can find huge collection of baby names </span>
                </Col>
              </Row>


              <Row className="items">
                <Col s={12} className="center">
                  <span style={{ color: '#fff', fontSize: '50px', paddingTop: '0px', fontFamily: 'titleFonts' }}>Features</span>
                </Col>

                <Col m={6} className="itemDiv">
                  <img src={require('../img/right.png')} className="rightIcon" />
                  <span className="itemText left">80,000+ Names</span>
                </Col>
                <Col m={6} className="itemDiv">
                  <img src={require('../img/right.png')} className="rightIcon" />
                  <span className="itemText left">Search By Religion</span>
                </Col>
                <Col m={6} className="itemDiv">
                  <img src={require('../img/right.png')} className="rightIcon" />
                  <span className="itemText left">Search By Rashi</span>
                </Col>
                <Col m={6} className="itemDiv">
                  <img src={require('../img/right.png')} className="rightIcon" />
                  <span className="itemText left">Detaild Description</span>
                </Col>
                <Col m={6} className="itemDiv">
                  <img src={require('../img/right.png')} className="rightIcon" />
                  <span className="itemText left">Filter By Gender&nbsp;</span>
                </Col>
                <Col m={6} className="itemDiv">
                  <img src={require('../img/right.png')} className="rightIcon" />
                  <span className="itemText left">Manage Favourites</span>
                </Col>

              </Row>

              <Row className="dataDiv">
                <Col s={12} className="center">
                  <span style={{ color: '#000', fontSize: '50px', paddingTop: '0px', fontFamily: 'titleFonts' }}>Search For Names</span>

                </Col>
                <Col m={6} style={{ marginTop: '20px' }}>

                  <Row style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', flexWrap: 'wrap' }}>
                    {
                      this.state.letters.map((val, i) => {
                        return (
                          <div key={i} className={this.state.first == (val) ? "letterBoxSelected" : "letterBox"} onClick={() => { this._selectFirst(val) }}>
                            <p style={{ color: '#fff', fontSize: '25px' }}>{val}</p>
                          </div>
                        )
                      })
                    }
                  </Row>


                  <Row style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', flexWrap: 'wrap' }}>
                    {
                      this.state.religions.map((val, i) => {
                        return (
                          <div key={i} className={this.state.selectedReligion == (val) ? "religionBoxSelected" : "religionBox"} onClick={() => { this._selectReligion(val) }}>
                            <p style={{ color: '#fff', fontSize: '15px' }}>{val}</p>
                          </div>
                        )
                      })
                    }
                  </Row>

                  <Row style={{ marginTop: '50px' }}>
                    <Col s={4}>
                      <div className="center">
                        <img src={require('../img/boy.png')} className={this.state.gender == "Boy" ? "genderImgSelected" : "genderImg"} onClick={() => { this._selectGender("Boy") }}></img>
                      </div>
                    </Col>
                    <Col s={4}>
                      <div className="center">
                        <img src={require('../img/girl.png')} className={this.state.gender == "Girl" ? "genderImgSelected" : "genderImg"} onClick={() => { this._selectGender("Girl") }}></img>
                      </div>
                    </Col>
                    <Col s={4}>
                      <div className="center">
                        <img src={require('../img/unisex.png')} className={this.state.gender == "Unisex" ? "genderImgSelected" : "genderImg"} onClick={() => { this._selectGender("Unisex") }}></img>
                      </div>
                    </Col>
                  </Row>

                </Col>
                <Col s={1} m={1}></Col>
                <Col m={4} s={10} style={{ marginTop: '20px', height: '700px', overflow: 'auto', padding: "0px", paddingRight: '8px' }}>
                  {
                    this.state.listLoaded ?
                      <InfiniteScroll
                        pageStart={0}
                        style={{ paddingLeft: '8px' }}
                        loadMore={this.state.hasNextData ? this._load : () => { }}
                        threshold={300}
                        hasMore={!this.state.loadNext}
                        loader={this.state.loadNext ? <div className="loader" key={0}>Loading ...</div> : <p></p>}
                        useWindow={false}
                      >
                        <Collapsible defaultActiveKey={0}>
                          {
                            this.state.data.map(function (val, i) {

                              return (
                                <CollapsibleItem header={val.name} key={i}>
                                  <Row>
                                    <Col m={6}><span style={{ color: '#000', fontWeight: 'bold' }}>Meaning</span></Col>
                                    <Col m={6}><span>{val.meaning}</span></Col>
                                  </Row>
                                  <Row>
                                    <Col m={6}><span style={{ color: '#000', fontWeight: 'bold' }}>Sex</span></Col>
                                    <Col m={6}><span>{val.gender}</span></Col>
                                  </Row>
                                  <Row>
                                    <Col m={6}><span style={{ color: '#000', fontWeight: 'bold' }}>Origin</span></Col>
                                    <Col m={6}><span>{val.origin}</span></Col>
                                  </Row>
                                  <Row>
                                    <Col m={6}><span style={{ color: '#000', fontWeight: 'bold' }}>Religion</span></Col>
                                    <Col m={6}><span>{val.religion}</span></Col>
                                  </Row>
                                </CollapsibleItem>
                              )
                            })
                          }
                        </Collapsible>
                        {
                          this.state.loadNext ?
                            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                              <Loader type="ThreeDots" color="#00BFFF" height={100} width={100} />
                            </div> :
                            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                              <p style={{ fontSize: '20px', color: '#f00' }}>Data Not Found</p>
                            </div>
                        }
                      </InfiniteScroll> :
                      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                        <Loader type="Rings" color="#00BFFF" height={100} width={100} />
                      </div>
                  }

                </Col>

              </Row>

              <Row className="contact">
                <Col s={12} className="center">
                  <span style={{ color: '#fff', fontSize: '50px', paddingTop: '0px', fontFamily: 'titleFonts' }}>Contact Us</span>
                </Col>
                <Col m={6}>
                  <Input s={6} label="First Name" className="input" />
                  <Input s={6} label="Last Name" className="input" />
                  <Input type="email" label="Email" s={12} className="input" onChange={(e) => { this.setState({ email: e }) }} />

                  <Input type='textarea' label="Message" s={12} className="input" />
                  <div className="center">
                    <Button waves='light' s={12} onClick={function () {
                      if (this.state.email != null) {
                        this.setState({ show: true });
                      }
                    }.bind(this)}>Submit</Button>
                  </div>
                  <SweetAlert
                    show={this.state.show}
                    title="Done"
                    text="Your form has been successfully submited"

                    onConfirm={function () {
                      this.setState({ show: false });
                    }.bind(this)}
                    onEscapeKey={() => this.setState({ show: false })}
                    onOutsideClick={() => this.setState({ show: false })}
                  />
                </Col>
                <Col m={6} className="hide-on-small-only">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d241317.1164782773!2d72.74075507665663!3d19.082197578739553!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c6306644edc1%3A0x5da4ed8f8d648c69!2sMumbai%2C+Maharashtra!5e0!3m2!1sen!2sin!4v1531030874700" className="map"></iframe>
                </Col>
              </Row>
            </div>
        }
      </div>
    );
  }
}

export default App;
